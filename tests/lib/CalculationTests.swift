//
//  CalculationTests.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

import XCTest
@testable import app

class CalculationTests: XCTestCase {
    
    func testCalculationAngle() {
        let p1 = CGPoint(x: 100, y: 100)
        let p2 = CGPoint(x: 50, y: 50)
        let center = CGPoint(x: 0, y: 0)
        let angle = Calculation.angle(p1: p1, p2: p2, center: center)
        
        XCTAssertEqual(angle, 0)
    }
}
