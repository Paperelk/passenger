//
//  MainScreenViewTests.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

import XCTest
@testable import app

class MainScreenViewTests: XCTestCase {
    
    func testScreenViewInitial() {
        let view = Main.ScreenView(frame: UIScreen.main.bounds)
        view.update(.initial)
        
        // Do something like screenshot test here
        XCTAssert(true)
    }
    
    func testScreenViewDriving() {
        let view = Main.ScreenView(frame: UIScreen.main.bounds)
        view.update(.drivingTo(
            coords: Main.ScreenView.Props.Coords(x: 1, y: 1),
            completion: {})
        )
        
        // Do something here
        XCTAssert(true)
    }
}
