//
//  MainPresenterTests.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

import XCTest
@testable import app

class MainPresenterTests: XCTestCase {
    
    func testReduce() {
        var props: Main.Props = .initial
        
        props = Main.Presenter.reduce(action: .start, feedback: { _ in })
        if case .waitForCoords = props {
            XCTAssert(true)
        } else {
            XCTFail()
        }
        
        props = Main.Presenter.reduce(action: .pickCoords(Main.Props.Coords(x: 1, y: 1)), feedback: { _ in })
        if case .drivingTo = props {
            XCTAssert(true)
        } else {
            XCTFail()
        }
        
        props = Main.Presenter.reduce(action: .didEndDriving, feedback: { _ in })
        if case .waitForCoords = props {
            XCTAssert(true)
        } else {
            XCTFail()
        }
    }
}
