//
//  MainScreenFactory.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

extension Main {
    
    static func createScreen() -> Main.Screen {
        return Screen(presenter: ReduxPresenter(
            reduce: Main.Presenter.reduce
        ))
    }
}
