//
//  MainScreen.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

import UIKit

extension Main {
    
    final class Screen: UIViewController {
        
        private let presenter: PresenterType
        
        init(presenter: PresenterType) {
            self.presenter = presenter
            super.init(nibName: nil, bundle: nil)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func loadView() {
            let view = ScreenView(frame: UIScreen.main.bounds)
            presenter.onDidChange = view.update
            self.view = view
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            presenter.sendAction(.start)
        }
    }
}
