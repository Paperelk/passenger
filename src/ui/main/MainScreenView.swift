//
//  MainScreenView.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

import UIKit

extension Main {
    
    final class ScreenView: UIView {
        
        enum Props {
            case initial
            case waitForCoords(pickCoords: (Coords) -> Void)
            case drivingTo(coords: Coords, completion: () -> Void)
            
            struct Coords {
                let x: CGFloat
                let y: CGFloat
            }
        }
        
        private weak var destinationView: UIView?
        private let destinationViewSize: CGFloat = 20.0
        private weak var carView: UIImageView!
        private let carViewSize = CGSize(width: 20, height: 60)
        
        private var props: Props = .initial
        
        func update(_ props: Props) {
            switch props {
            case .initial:
                isUserInteractionEnabled = false
            case .waitForCoords:
                clearDrivingScene()
                isUserInteractionEnabled = true
            case let .drivingTo(coords, driveCompletion):
                guard case .waitForCoords = self.props else {
                    return
                }
                isUserInteractionEnabled = false
                createDrivingScene(coords, driveCompletion)
            }
            self.props = props
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            backgroundColor = .white
            let car = UIImageView(frame: CGRect(origin: .zero, size: carViewSize))
            carView = car
            carView.image = UIImage(named: "ic_car")
            carView.contentMode = .scaleAspectFill
            carView.center = center
            addSubview(carView)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            super.touchesBegan(touches, with: event)
            guard let touch = Array(touches).last else {
                return
            }
            let point = touch.location(in: self)
            if case let .waitForCoords(action) = props {
                action(Props.Coords(x: point.x, y: point.y))
            }
        }
    }
}

extension Main.ScreenView {
    
    fileprivate func carTop(in view: UIView) -> CGPoint {
        let top = CGPoint(x: carView.bounds.midX, y: carView.bounds.minY)
        return carView.convert(top, to: view)
    }
    
    fileprivate func createDestinationPoint(_ coords: Props.Coords) -> UIView {
        let destination = UIView(frame: CGRect(
            origin: CGPoint(
                x: coords.x - destinationViewSize / 2,
                y: coords.y - destinationViewSize / 2
            ),
            size: CGSize(
                width: destinationViewSize,
                height: destinationViewSize
            )
        ))
        destination.backgroundColor = .red
        destination.layer.cornerRadius = destinationViewSize / 2
        return destination
    }
    
    fileprivate func clearDrivingScene() {
        destinationView?.removeFromSuperview()
    }
    
    fileprivate func createDrivingScene(_ coords: Props.Coords, _ completion: @escaping () -> Void) {
        let destination = createDestinationPoint(coords)
        self.destinationView = destination
        addSubview(destination)
        
        let p1 = carTop(in: self)
        let p2 = destination.center
        let center = carView.center
        let angle = Calculation.angle(p1: p1, p2: p2, center: center)
        
        let animator1 = UIViewPropertyAnimator(duration: 1.5, curve: .linear) {
            self.carView.rotate(angle: angle)
        }
        let animator2 = UIViewPropertyAnimator(duration: 2.5, curve: .easeInOut) {
            self.carView.center = CGPoint(x: coords.x, y: coords.y)
        }
        animator1.addCompletion { _ in animator2.startAnimation() }
        animator2.addCompletion { _ in completion() }
        animator1.startAnimation()
    }
}
