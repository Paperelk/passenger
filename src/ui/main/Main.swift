//
//  Main.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

enum Main {
    
    enum Action {
        case start
        case pickCoords(Props.Coords)
        case didEndDriving
    }
    
    typealias Props = ScreenView.Props
    typealias PresenterType = ReduxPresenter<Action, Props>
}
