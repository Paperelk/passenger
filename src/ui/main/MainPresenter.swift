//
//  MainPresenter.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

extension Main {
    
    enum Presenter {
        static func reduce(action: Action, feedback: @escaping (Action) -> Void) -> Props {
            switch action {
            case .start, .didEndDriving:
                return Props.waitForCoords(
                    pickCoords: { feedback(.pickCoords($0)) }
                )
                
            case let .pickCoords(coords):
                return Props.drivingTo(
                    coords: coords,
                    completion: { feedback(.didEndDriving) }
                )
            }
        }
    }
}
