//
//  Calculation.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

import CoreGraphics

enum Calculation {
    
    // Angle in radians between two vectors, created by p1, p2, center.
    static func angle(p1: CGPoint, p2: CGPoint, center: CGPoint) -> CGFloat {
        let v1 = CGVector(dx: p1.x - center.x, dy: p1.y - center.y)
        let v2 = CGVector(dx: p2.x - center.x, dy: p2.y - center.y)
        let angle = atan2(v2.dy, v2.dx) - atan2(v1.dy, v1.dx)
        return angle
    }
}
