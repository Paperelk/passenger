//
//  UIView+Rotate.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

import UIKit

extension UIView {
    func rotate(angle: CGFloat) {
        let rotation = self.transform.rotated(by: angle)
        self.transform = rotation
    }
}
