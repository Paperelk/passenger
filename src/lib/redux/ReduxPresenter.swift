//
//  ReduxPresenter.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

final class ReduxPresenter<Action, Props>: ReduxPresenterType {
    
    var onDidChange: (Props) -> Void = { _ in }
    private let reduce: (Action, @escaping (Action) -> Void) -> Props
    
    init(reduce: @escaping (Action, @escaping (Action) -> Void) -> Props) {
        self.reduce = reduce
    }
    
    func sendAction(_ action: Action) {
        onDidChange(reduce(action, self.sendAction))
    }
}
