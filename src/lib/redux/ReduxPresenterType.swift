//
//  ReduxPresenterType.swift
//  Copyright © 2019 someorg.name. All rights reserved.
//

protocol ReduxPresenterType {
    associatedtype Props
    associatedtype Action
    
    var onDidChange: (Props) -> Void { get set }
    
    init(reduce: @escaping (_ action: Action, _ feedback: @escaping (Action) -> Void) -> Props)
    func sendAction(_ action: Action)
}
